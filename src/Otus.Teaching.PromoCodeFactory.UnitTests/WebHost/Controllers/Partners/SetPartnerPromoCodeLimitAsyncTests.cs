﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Threading.Tasks;
using Xunit;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using FluentAssertions;
using System.Collections.Generic;
using AutoFixture.AutoMoq;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();
        private PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        private Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("802af5d9-40cf-4df7-8246-9270d5fc4013"),
                Name = "Магнит",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("3d178272-ffa1-41bf-bbd4-18edcf779258"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        [Fact] /*1*/
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotExist_ReturnNotFound()
        {
            //Arrange
            var id = Guid.NewGuid();
            Partner emptyPartner = null;

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(emptyPartner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, null);

            //Assert FluentAssertions
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact] /*2*/
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsBlocked_ReturnBadRequest()
        {
            //Arrange
            var id = Guid.Parse("022df71f-045f-43fb-8fbc-0f62459c7ea0");
            var partner = new PartnerBuilder()
                .WithPartnerId(id)
                .WithIsActive(false)
                .Build();

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, null);

            //Assert FluentAssertions
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory] /*3*/
        [InlineData(false, 0)]
        [InlineData(true, 5)]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ClearPromocodesIfEndLimit(bool setDate, int numberIssuedPromoCodes)
        {
            //Arrange
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 5;
            if (setDate)
                partner.PartnerLimits.First().CancelDate = DateTime.Now.AddDays(-1);

            var request = new SetPartnerPromoCodeLimitRequest { Limit = 10 };

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
          
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);

        }

        [Fact] /*4*/
        public async Task SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_SetLimitCancelDate()
        {
            //Arrange
            //partner have one limit Id = "3d178272-ffa1-41bf-bbd4-18edcf779258"
            var partner = CreateBasePartner();
            var request = new SetPartnerPromoCodeLimitRequest { Limit = 10 };

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.First(p => p.Id == Guid.Parse("3d178272-ffa1-41bf-bbd4-18edcf779258")).CancelDate
                .Should().HaveValue();
        }

        [Fact] /*5*/
        public async Task SetPartnerPromoCodeLimitAsync_RequestHasLimit_ReturnBadRequest()
        {
            //Arrange
            var partner = CreateBasePartner();
            var request = new SetPartnerPromoCodeLimitRequest { Limit = 0 };

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}