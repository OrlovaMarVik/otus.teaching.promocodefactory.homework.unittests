﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Guid _partnerId;
        private bool _partnerIsActive;

        public PartnerBuilder WithPartnerId (Guid id)
        {
            _partnerId = id;
            return this;
        }

        public PartnerBuilder WithIsActive(bool isActive)
        {
            _partnerIsActive = isActive;
            return this;
        }

        public Partner Build()
        {
            return new Partner()
            {
                Id = _partnerId,
                IsActive = _partnerIsActive
            };
        }

    }
}
