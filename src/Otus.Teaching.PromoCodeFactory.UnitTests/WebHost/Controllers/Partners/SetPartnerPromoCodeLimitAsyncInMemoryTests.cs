﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncInMemoryTests : IClassFixture<TestFixture_InMemory>
    {
        private IRepository<Partner> _partnersRepository;
        private IRepository<PartnerPromoCodeLimit> _partnerLimitsRepository;
        private PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncInMemoryTests(TestFixture_InMemory testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
            _partnersRepository = serviceProvider.GetService<IRepository<Partner>>();
            _partnerLimitsRepository = serviceProvider.GetService<IRepository<PartnerPromoCodeLimit>>();
            _partnersController = new PartnersController(_partnersRepository, _partnerLimitsRepository);
        }

        [Fact] /*6*/
        public async Task SetPartnerPromoCodeLimitAsync_NewLimit_SaveInDB()
        {
            //Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"); //issue partner from FakeData seed to DB
            var request = new SetPartnerPromoCodeLimitRequest { Limit = 10 };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var newLimit = await _partnersController.GetPartnerLimitAsync(
                Guid.Parse((result as CreatedAtActionResult).RouteValues.GetValueOrDefault("id").ToString()),
                Guid.Parse((result as CreatedAtActionResult).RouteValues.GetValueOrDefault("limitId").ToString()));

            //Assert
            newLimit.Should().BeOfType<ActionResult<PartnerPromoCodeLimit>>();

        }
    }
}
